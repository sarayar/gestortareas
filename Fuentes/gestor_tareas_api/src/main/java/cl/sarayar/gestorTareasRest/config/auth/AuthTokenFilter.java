package cl.sarayar.gestorTareasRest.config.auth;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import cl.sarayar.gestorTareasRest.services.UsuariosService;
import cl.sarayar.gestorTareasRest.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;

public class AuthTokenFilter extends OncePerRequestFilter {

	@Autowired
	private JwtUtils jwtUtils;
	@Autowired
	private UsuariosService usService;
	
	private  UsernamePasswordAuthenticationToken getAuthentication(final String token,
			final UserDetails userDetails) {
		final JwtParser jwtParser = Jwts.parser().setSigningKey(jwtUtils.getSigningKey());
		final Jws<Claims> claimsJws = jwtParser.parseClaimsJws(token);
		final Claims claims = claimsJws.getBody();
		
		final Collection<SimpleGrantedAuthority> authorities = Collections.emptyList();
		return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
	}
	

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
	    try {
	    	 
	        String jwt = parseJwt(request);
	       
	        if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
	          String username = jwtUtils.getUserNameFromJwtToken(jwt);

	          UserDetails userDetails = usService.loadUserByUsername(username);
	          UsernamePasswordAuthenticationToken authentication = this.getAuthentication(jwt, userDetails);
	          authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

	          SecurityContextHolder.getContext().setAuthentication(authentication);
	        }
	      } catch (Exception e) {
	        logger.error("Cannot set user authentication: {}", e);
	      }

	      filterChain.doFilter(request, response);
	}

	private String parseJwt(HttpServletRequest request) {
		String headerAuth = request.getHeader("Authorization");
		
		if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
			return headerAuth.substring(7, headerAuth.length());
		}

		return null;
	}

}
