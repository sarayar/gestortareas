package cl.sarayar.gestorTareasRest.test.controllers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.sarayar.gestorTareasRest.entities.Tarea;
import cl.sarayar.gestorTareasRest.services.TareasService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class TareasControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private TareasService tareasService;
	@Autowired
	private ObjectMapper mapper;
	
	
	@Test
	public void whenTareasGet_thenOk() throws Exception {
		
		List<Tarea> tareas = new ArrayList<>();
		when(tareasService.findAll()).thenReturn(tareas);
		
		this.mockMvc.perform(get("/tareas/get"))
			.andDo(print()).andExpect(status().isOk());
	}
	
	@Test
	public void whenTareasUpdateAndTareaIsValid_thenOk() throws Exception {
		Tarea t = new Tarea();
		t.setId("1");
		t.setIdentificador(123);
		t.setFechaCreacion(LocalDateTime.now());
		t.setDescripcion("Test");
		t.setVigente(true);
		when(tareasService.save(ArgumentMatchers.any(Tarea.class))).thenReturn(t);
		when(tareasService.findById(ArgumentMatchers.anyString())).thenReturn(t);
		this.mockMvc
		.perform(post("/tareas/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(t))
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print()).andExpect(status().isOk());
	}
	
	@Test
	public void whenTareasUpdateAndTareaIsValid_thenNok() throws Exception {
		Tarea t = new Tarea();
		t.setId("1");
		t.setIdentificador(123);
		t.setFechaCreacion(LocalDateTime.now());
		t.setDescripcion("Test");
		t.setVigente(true);
		when(tareasService.save(ArgumentMatchers.any(Tarea.class))).thenReturn(t);
		when(tareasService.findById(ArgumentMatchers.anyString())).thenReturn(null);
		this.mockMvc
		.perform(post("/tareas/update")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(t))
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print()).andExpect(status().is5xxServerError());
	}
	
	
	@Test
	public void whenTareasSaveAndTareaValid_thenOk() throws Exception{
		
		Tarea t = new Tarea();
		t.setId("1");
		t.setIdentificador(123);
		t.setFechaCreacion(LocalDateTime.now());
		t.setDescripcion("Test");
		t.setVigente(true);
		
		when(tareasService.save(ArgumentMatchers.any(Tarea.class))).thenReturn(t);
		
		this.mockMvc
		.perform(post("/tareas/post")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(t))
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print()).andExpect(status().isOk());
	}
	
	@Test
	public void whenTareasSaveAndTareaInvalid_thenNOk() throws Exception{
		
		Tarea t = new Tarea();
		
		t.setFechaCreacion(LocalDateTime.now());
		t.setDescripcion(null);
		
		
		when(tareasService.save(ArgumentMatchers.any(Tarea.class))).thenReturn(t);
		
		this.mockMvc
				.perform(post("/tareas/post")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(t))
				.accept(MediaType.APPLICATION_JSON))
				
				.andDo(print())
				.andExpect(status().is4xxClientError());
	}
	
	
	@Test
	public void whenTareasDelete_thenNok() throws Exception {
		
	
		when(tareasService.remove(ArgumentMatchers.anyString())).thenThrow(IllegalArgumentException.class);
		
		this.mockMvc.perform(delete("/tareas/delete/2"))
			.andDo(print()).andExpect(status().is5xxServerError());
	}
	
	@Test
	public void whenTareasDelete_thenOk() throws Exception {
		
	
		when(tareasService.remove(ArgumentMatchers.anyString())).thenReturn(true);
		
		this.mockMvc.perform(delete("/tareas/delete/1a"))
			.andDo(print()).andExpect(status().isOk());
	}
	
}
