import React from 'react'
import { AppRouter } from '../routers/AppRouter'
import { NavBar } from '../components/templates/NavBar'
import { CargandoComponent } from '../components/ui/CargandoComponent'

export const HomeContainer = () => {
    return (
        <>
          <CargandoComponent></CargandoComponent>
          <header>
              <NavBar></NavBar>
          </header>  
          <main className="container-fluid">
            <AppRouter></AppRouter>
          </main>
        </>
    )
}
