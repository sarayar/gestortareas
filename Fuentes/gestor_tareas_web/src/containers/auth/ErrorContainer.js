import React from 'react'

export const ErrorContainer = () => {
    return (
        <div className="row">
        <div className="col-12 col-md-8 col-lg-6 mx-auto text-center alert alert-danger mt-5">
            <h5>No tiene privilegios suficientes para acceder a la funcionalidad</h5>
        </div>
        </div>

    )
}
