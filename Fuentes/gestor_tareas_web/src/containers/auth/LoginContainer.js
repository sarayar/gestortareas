import React, { useRef, useState } from 'react'
import './Login.scss';
import { useDispatch } from 'react-redux';
import { Toast } from 'primereact/toast';
import { iniciarSesion } from '../../actions/authActions';
import { finalizaLoading, loading } from '../../actions/uiActions';
import { CargandoComponent } from '../../components/ui/CargandoComponent';
export const LoginContainer = () => {
    const toast = useRef();
    const dispatch = useDispatch();
    const [correo, setCorreo] = useState('');
    const [clave, setClave] = useState('');
    const handleSubmit = async (e)=>{

        e.preventDefault();
        if(correo.trim() === '' || clave.trim() === '' ){
            toast.current.show({severity:'warn',summary:'Debe Ingresar sus credenciales'
            ,detail:"Para iniciar sesión debe ingresar sus credenciales"});
           
        }else{
            dispatch(loading());
            const respuesta = await dispatch(iniciarSesion({correo,clave}));
            dispatch(finalizaLoading());
            if(!respuesta){          
                toast.current.show({severity:'error',summary:'Error de Inicio de Sesión',detail:"Credenciales incorrectas"});
            }
        }
    }


    return <div className="login-container bg-primary">
        <CargandoComponent></CargandoComponent>
        <Toast ref={toast} />
        <div className="wrapper fadeInDown">
            <div id="formContent">
                <h2 className="active">Iniciar Sesión </h2>
                <form onSubmit={handleSubmit}>
                    <input type="text" id="login" value={correo} onChange={(e)=>setCorreo(e.target.value)}  className="fadeIn second" name="login" placeholder="Nombre de Usuario" />
                    <input type="password" id="password" className="fadeIn third" name="login" value={clave} onChange={(e)=>setClave(e.target.value)} placeholder="Contraseña" />
                    <input type="submit" className="fadeIn fourth" value="Entrar" />
                </form>
            </div>
        </div>
    </div>

}
