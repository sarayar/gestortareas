import React, { useEffect, useState } from 'react'
import { useDispatch,useSelector } from 'react-redux';
import { TareasFormModalComponent } from '../components/tareas/TareasFormModalComponent';
import { TareasTableComponent } from '../components/tareas/TareasTableComponent';
import {startCargarTareas,mostrarModalTarea} from '../actions/tareasActions'
export const TareasContainer= () => {

    const [tareaEditada, setTareaEditada] = useState(null);
    const dispatch = useDispatch();
    const {tareas} = useSelector(state => state.tareas);
    const handleNuevaClick = (e)=>{
        setTareaEditada(null);
        dispatch(mostrarModalTarea());
    }
    const handleEditarTarea = (tarea)=>{
        setTareaEditada(tarea);
        dispatch(mostrarModalTarea());
    }
    useEffect(() => {
        dispatch(startCargarTareas())
    }, [dispatch]);
    const {mostrarModal} = useSelector(state => state.tareas);
    return (
        <>
            <div className="row mt-5">
                <div className="col-12 col-md-8 col-lg-6 mx-auto">
                    <h1>Tareas</h1>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col-12 col-md-8 col-lg-6 mx-auto">
                    <button type="button" onClick={handleNuevaClick} className="btn btn-dark">Crear Nueva</button>
                </div>
            </div>
            <div className="row mt-5">
                <div className="col-12 col-md-8 col-lg-6 mx-auto">
                    <TareasTableComponent onEditarTarea={handleEditarTarea} tareas={tareas}></TareasTableComponent>
                </div>
            </div>
            {<TareasFormModalComponent  mostrar={mostrarModal} tareaActual={tareaEditada}></TareasFormModalComponent>}
        </>
    )
}
