import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import {BrowserRouter as Router, Switch, Redirect} from 'react-router-dom';
import { cargarUsuario } from '../actions/authActions';
import { LoginContainer } from '../containers/auth/LoginContainer';
import { HomeContainer } from '../containers/HomeContainer';
import usuariosService from '../services/usuariosService';
import {PublicRoute} from './PublicRoute';
import {PrivateRoute} from './PrivateRoute';
import { CargandoComponent } from '../components/ui/CargandoComponent';
export const BaseRouter= () => {
    const {usuario} = useSelector(state => state.auth);
    const dispatch = useDispatch();

    useEffect(() => {
      const usuarioAux = usuariosService.getUsuario();
     
      dispatch(cargarUsuario(usuarioAux));
      

    }, [dispatch]);
    //Si aun no se evalua el estado del usuario, se devuelve el componente Cargando
   if(usuario === undefined){
     return <CargandoComponent></CargandoComponent>
   }
    return (
        <Router>
        <div>
          <Switch>
            <PublicRoute isAuthenticated={usuario != null} exact path="/login" component={LoginContainer}></PublicRoute>
            <PrivateRoute isAuthenticated={usuario!=null}  path="/" component={HomeContainer}></PrivateRoute>
            <Redirect to="/tareas"></Redirect>
          </Switch>
        </div>
      </Router>
    )
}
