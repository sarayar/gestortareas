import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect, Switch } from 'react-router-dom'
import { CargandoComponent } from '../components/ui/CargandoComponent'
import {PrivateRoute} from './PrivateRoute';
import {TareasContainer} from '../containers/TareasContainer';
export const AppRouter = () => {
    const {usuario} = useSelector(state => state.auth);
    //Si aun no se evalua el estado del usuario, se devuelve el componente Cargando
    if(usuario === undefined){
        return <CargandoComponent></CargandoComponent>
    }
    return (
        <Switch>
            <PrivateRoute isAuthenticated={usuario != null} exact path="/tareas" component={TareasContainer}></PrivateRoute>
            <Redirect to="/tareas"></Redirect>
        </Switch>
    )
}
