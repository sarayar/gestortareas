
import './App.scss';
import 'bootstrap';
import {Provider} from 'react-redux';
import { BaseRouter } from './routers/BaseRouter';
import { store } from './store/store';

function App() {
  return (
    <Provider store={store}>
      <BaseRouter></BaseRouter>
    </Provider>
  );
}

export default App;
