import usuariosService from "../services/usuariosService";
import { types } from "./types";
import { finalizaLoading, loading } from "./uiActions";

export const getTodosUsuarios = (usuarios)=>({
    type:types.usuariosGetAll,
    payload:usuarios
});


export const startGetTodosUsuarios = ()=>{
    return async (dispatch)=>{
        try{
            const usuarios = await usuariosService.getAll();
            dispatch(getTodosUsuarios(usuarios));
            return true;
        }catch(e){
            return false;
        }
    };
};

export const startRegistrarUsuario = (usuario)=> async (dispatch)=>{
    try{
        const resp = await usuariosService.registrar(usuario);
        dispatch(registrarUsuario(resp));
        return true;
    }catch(e){
        return false;
    }
}

export const startActualizarUsuario = (usuario)=> async(dispatch)=>{
    try{
        const resp = await usuariosService.actualizar(usuario);
        dispatch(actualizarUsuario(resp));
        return true;
    }catch(e){
        return false;
    }
}

export const actualizarUsuario = (usuario)=>({
    type: types.usuariosActualizarUsuario,
    payload:usuario
});

export const registrarUsuario = (usuario)=>({
    type: types.usuariosRegistrar,
    payload:usuario
});
