import usuariosService from "../services/usuariosService";
import { types } from "./types";


export const loginUsuario = (usuario)=>({
    type: types.usuariosLoginOk,
    payload: usuario
});

export const cerrarSesion = ()=>{
    usuariosService.logout();
    return {
        type:types.usuariosCerrarSesion
    }
};

export const errorToken=()=>({
    type:types.usuariosErrorToken
});

export const cargarUsuario = (usuario)=>({
    type:types.usuariosCargarUsuario,
    payload:usuario
});

export const iniciarSesion = (usuario)=> async (dispatch)=>{
    try{
        const resp = await usuariosService.login(usuario);
        if(resp){
            dispatch(loginUsuario(resp));
            return resp;
        }else{
            return false;
        }
    }catch(e){
        return false;
    }
};
