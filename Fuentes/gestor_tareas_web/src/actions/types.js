export const types={

    //UI
    uiLoading: '[UI] Loading',
    uiFinalizaLoading: '[UI] Finaliza Loading',
    //Usuarios
    usuariosLoginOk: '[Usuarios] Login',
    usuariosGetAll: '[Usuarios] Obtener todos',
    usuariosRegistrar: '[Usuarios] Registrar',
    usuariosCerrarSesion: '[Usuarios] Cerrar Sesion',
    usuariosErrorToken: '[Usuarios] Error de Token',
    usuariosCargarUsuario: '[Usuarios] Cargar Usuario',
    usuariosActualizarUsuario: '[Usuarios] Actualizar Usuario',
    //Tareas
    tareasCargar: '[Tareas] Obtener tareas',
    tareasAgregar: '[Tareas] Agregar tarea',
    tareasEliminar: '[Tareas] Eliminar tarea',
    tareasActualizar: '[Tareas] Actualizar tarea',
    tareasCambiarEstadoModal: '[Tareas] Cambiar Estado Modal',
};