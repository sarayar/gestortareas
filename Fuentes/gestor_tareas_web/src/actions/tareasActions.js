import tareasService from "../services/tareasService";
import { types } from "./types";
import { finalizaLoading, loading } from "./uiActions";
import uiService from "../services/uiService";
const agregarTarea = (tarea)=>({
    type: types.tareasAgregar,
    payload:tarea
});

const cargarTareas = (tareas)=>({
    type:types.tareasCargar,
    payload: tareas
});


const actualizarTarea = (tarea)=>({
    type: types.tareasActualizar,
    payload:tarea
});

const eliminarTarea = (id)=>({
    type:types.tareasEliminar,
    payload:id
});


export const mostrarModalTarea = ()=>({
    type: types.tareasCambiarEstadoModal,
    payload:true
});

export const ocultarModalTarea = ()=>({
    type: types.tareasCambiarEstadoModal,
    payload:false
})


export const startCargarTareas =  ()=> async (dispatch)=>{
    try{
        const tareas = await tareasService.getAll();
        if(tareas){
            dispatch(cargarTareas(tareas));
        }
        return true;
    }catch(e){
        return false;
    }

};

export const startEliminarTarea = (tarea)=> async (dispatch)=>{
    dispatch(loading());
    try{
        const resp = await tareasService.delete(tarea.id);
        if(resp){
            dispatch(eliminarTarea(tarea.id));
        }
        return resp;
    }catch(e){
        
        return false;
    }finally{
        dispatch(finalizaLoading());
    }

}

export const startAgregarTarea = (tarea)=>{
    return async (dispatch)=>{
        dispatch(loading());
        tarea = await tareasService.save(tarea);
        dispatch(agregarTarea(tarea));
        dispatch(startCargarTareas());
        dispatch(finalizaLoading());
        await uiService.mostrarMensaje(`Tarea ${tarea.identificador} Registrada`, 'success', ()=>{
            dispatch(ocultarModalTarea());
        });
    }
}
export const startActualizarTarea = (tarea)=>{
    return async (dispatch)=>{
        dispatch(loading());
        await tareasService.update(tarea);
        dispatch(actualizarTarea(tarea));
        dispatch(startCargarTareas());
        dispatch(finalizaLoading());
        await uiService.mostrarMensaje(`Tarea ${tarea.identificador} Actualizada`, 'success', ()=>{
            dispatch(ocultarModalTarea());
        });
    }
}
