import { types } from "./types";

export const loading=()=>({
    type: types.uiLoading
});

export const finalizaLoading = ()=>({
    type:types.uiFinalizaLoading
});
