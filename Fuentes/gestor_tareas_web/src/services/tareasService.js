
import BaseService from './baseService';
const URL_API = process.env.REACT_APP_API_URL;
class TareasService extends BaseService {
    getAll = async () => {
        const resp = await this.get(`${URL_API}/tareas/get`);
        return resp?resp.data:null;
    }

    save = async (categoria) => {
        const resp = await this.post(`${URL_API}/tareas/post`, categoria, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return resp? resp.data:null;
    }

    delete = async (id)=>{
        const resp = await this.del(`${URL_API}/tareas/delete/${id}`);
        return resp? resp.data:null;
    }

    update = async (categoria) => {
        const resp = await this.post(`${URL_API}/tareas/update`, categoria, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return resp?resp.data:null;
    }
}

export default new TareasService();