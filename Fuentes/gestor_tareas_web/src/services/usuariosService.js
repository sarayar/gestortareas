import axios from 'axios';
import BaseService from './baseService';
const URL_API = process.env.REACT_APP_API_URL;

class UsuariosService extends BaseService{

    getUsuario(){
        return JSON.parse(localStorage.getItem("usuario"));
    }

    async getAll(){
        const resp = await this.get(`${URL_API}/usuarios/get`);
        return resp.data;
    }

    async login({correo,clave}){
       const resp = await axios.post(`${URL_API}/usuarios/login`, {correo,clave});
    
       if(resp.data.token){
            let usuario = resp.data.usuario;
            usuario.token = resp.data.token;
            delete usuario.clave;
            localStorage.setItem("usuario", JSON.stringify(usuario));
            return usuario; 
       } else {
           return null;
       }
    }

    logout(){
        localStorage.removeItem("usuario");
    }

    async registrar(usuario){
        const resp = await this.post(`${URL_API}/usuarios/registrar`,usuario,{
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return resp.data;
    }
    async actualizar(usuario){
        const resp = await this.post(`${URL_API}/usuarios/actualizar`,usuario,{
            headers: {
                'Content-Type': 'application/json'
            }
        });
        return resp.data;
    }

}

export default new UsuariosService();



