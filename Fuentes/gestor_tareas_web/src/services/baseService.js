import axios from "axios";

import { errorToken } from "../actions/authActions";
import { store } from "../store/store";

class BaseService{

    async get(url,options={headers:{}}){
        try{
            const {headers} = options; 
            if(options.responseType){
                return await axios.get(url,{headers:{...headers,...this.authHeader()},responseType:options.responseType});
            }else {
                return await axios.get(url,{headers:{...headers,...this.authHeader()}});
            }
        }catch(e){
            if(e?.response?.status === 401){
            
                store.dispatch(errorToken());
            }else{
                throw e;
            }
            
        }
    }

    async del(url,options={headers:{}}){
        try{
            const {headers} = options; 
            if(options.responseType){
                return await axios.delete(url,{headers:{...headers,...this.authHeader()},responseType:options.responseType});
            }else {
                return await axios.delete(url,{headers:{...headers,...this.authHeader()}});
            }
        }catch(e){
            if(e?.response?.status === 401){
                store.dispatch(errorToken()); 
            }else{
                throw e;
            }
            
        }
    }

    async post(url,datos,options={headers:{}}){
        try{
            const {headers} = options; 
            if(options.responseType){
                return await axios.post(url,datos,{headers:{...headers,...this.authHeader()},responseType:options.responseType});
            }else {
                return await axios.post(url,datos,{headers:{...headers,...this.authHeader()}});
            }
        }catch(e){
            if(e?.response?.status === 401){
                store.dispatch(errorToken()); 
            }else{
                throw e;
            }
            
        }
    }
    authHeader(){
        const usuario = JSON.parse(localStorage.getItem("usuario"));
        if(usuario && usuario.token){
            return {'Authorization': 'Bearer ' + usuario.token};
        }else{
            return {};
        }
    }
}

export default BaseService;