import Swal from 'sweetalert2';

class UiService {
    mostrarMensaje = async (mensaje, tipo, callback = null) => {
        await Swal.fire({ title: mensaje, icon: tipo });

        callback && callback();
    }
}

export default new UiService();