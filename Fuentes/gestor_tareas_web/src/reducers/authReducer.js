import { types } from "../actions/types"

const initialState = {
    usuario: undefined,
    mensajeError:null
}

export const authReducer =  (state=initialState, action)=>{
    switch(action.type){
        case types.usuariosLoginOk: 
            return {...state,mensajeError:null,usuario:action.payload};
        case types.usuariosCargarUsuario:
            return {...state,mensajeError:null,usuario:action.payload};
        case types.usuariosCerrarSesion:
            return {...state,usuario:null,mensajeError:null};
        case types.usuariosErrorToken:
                return {...state,usuario:null,mensajeError:null}
        default: return state;
    }
}