import { types } from "../actions/types";

const initialState = {
    loading:false
}
export const uiReducer= (state=initialState, action)=>{
    switch(action.type){
        case types.uiLoading: return {...state,loading:true};
        case types.uiFinalizaLoading: return {...state,loading:false};
        default:return state;
    }
};