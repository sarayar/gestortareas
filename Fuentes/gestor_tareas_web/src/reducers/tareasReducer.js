import { types } from "../actions/types";

const initialState = {
    tareas:[],
    mostrarModal:false,
};
export const tareasReducer = (state=initialState, action)=>{
    switch(action.type){
        case types.tareasAgregar:
            return {tareas:[...state.tareas,action.payload]};
        case types.tareasCargar:
            return {tareas:action.payload};
        case types.tareasEliminar:
            return {tareas:state.tareas.filter(c=>c.id!==action.payload)};
        case types.tareasActualizar:
            return {tareas:state.tareas.map(c=>
                c.id === action.payload.id ? action.payload:c  
            )};
        case types.tareasCambiarEstadoModal:
            return {...state, mostrarModal:action.payload}
        default:
            return state;
    }
}