import { types } from "../actions/types"

const initialState = {
    usuarios:[],
    privilegios:[]
}

export const usuariosReducer =  (state=initialState, action)=>{
    switch(action.type){
        case types.usuariosGetAll:
            return {...state,usuarios:action.payload};
        case types.usuariosRegistrar:
            return {...state,usuarios:[...state.usuarios,action.payload]};
        case types.usuariosActualizarUsuario:
            return {...state,usuarios:[...state.usuarios.map(u=>u.id === action.payload.id?action.payload:u)]};
        case types.usuariosTodosPrivilegios:
            return {...state,privilegios:action.payload};
        default: return state;
    }
}