import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moment from 'moment';

import '@testing-library/jest-dom';
import { TareasFormModalComponent } from '../../components/tareas/TareasFormModalComponent';
import { startActualizarTarea,startAgregarTarea,startCargarTareas,startEliminarTarea} from '../../actions/tareasActions';
import { act } from '@testing-library/react';
import Swal from 'sweetalert2';

jest.mock('sweetalert2', () => ({
    fire: jest.fn(),
}))
jest.mock('../../actions/tareasActions', () => ({
    startActualizarTarea: jest.fn(),
    startAgregarTarea: jest.fn(),
    startCargarTareas: jest.fn(),
    startEliminarTarea: jest.fn()
}))

const middlewares = [ thunk ];
const mockStore = configureStore( middlewares );
const tareaTest = {
    id:"1234",
    identificador: 1234,
    descripcion:"Test",
    vigente:true,
    fechaCreacion:new Date()
};
const initState = {
    tareas: {
        tareas:[],
        mostrarModal:false,
    }
};

const store = mockStore( initState );

store.dispatch = jest.fn();
const wrapper = mount(
    <Provider store={ store } >
        <TareasFormModalComponent tareaActual={null} mostrar={true} />
    </Provider>
);

describe("Pruebas de Tareas Form Modal ", ()=>{
    beforeEach(()=>{
        jest.clearAllMocks();
    });

    test("Debe mostrarse correctamente", ()=>{
        expect(wrapper).toMatchSnapshot();
    });
    test("Debe mostrar formulario registrar", ()=>{
        expect(wrapper.find('.p-dialog-title').text()).toBe("Registrar Tarea");
    });
    test("Al tener tarea debe mostrar formulario actualizar", ()=>{
        const editWrapper =  mount(
            <Provider store={ store } >
                <TareasFormModalComponent tareaActual={tareaTest} mostrar={true} />
            </Provider>
        );
        expect(editWrapper.find('.p-dialog-title')
            .text()).toBe(`Editar Tarea ${tareaTest.identificador}`);
    });

    test("Debe crear una nueva tarea", ()=>{
        const storeCrear = mockStore(initState);
        storeCrear.dispatch = jest.fn();
        const wrapperCrear = mount(
        <Provider store={ storeCrear } >
            <TareasFormModalComponent tareaActual={null} mostrar={true} />
        </Provider>)
        wrapperCrear.find('input[name="descripcion"]').simulate('change', {
            target:{
                name:"descripcion",
                value:"Test Prueba"
            }
        });

        wrapperCrear.find('button#boton-accion').simulate('click', {
            
        });
        expect(startAgregarTarea).toHaveBeenCalledWith({
            id:"",
            descripcion:"Test Prueba",
            vigente:false
        });
    });
});




