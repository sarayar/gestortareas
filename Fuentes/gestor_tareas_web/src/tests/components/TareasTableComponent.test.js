import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import '@testing-library/jest-dom';
import { TareasTableComponent } from '../../components/tareas/TareasTableComponent';
import { startActualizarTarea,startAgregarTarea,startCargarTareas,startEliminarTarea} from '../../actions/tareasActions';
import Swal from 'sweetalert2';

jest.mock('sweetalert2', () => ({
    fire: jest.fn(),
}))
jest.mock('../../actions/tareasActions', () => ({
    startActualizarTarea: jest.fn(),
    startAgregarTarea: jest.fn(),
    startCargarTareas: jest.fn(),
    startEliminarTarea: jest.fn()
}))

const middlewares = [ thunk ];
const mockStore = configureStore( middlewares );

const initState = {
    tareas: {
        tareas:[],
        mostrarModal:false,
    }
};

const store = mockStore( initState );

store.dispatch = jest.fn();

const tareasTest = [{id:1123,identificador:1, descripcion:"Test", vigente:false}];
const wrapper = mount(
    <Provider store={ store } >
        <TareasTableComponent tareas={tareasTest} />
    </Provider>
);

describe("Pruebas de Tareas Table Component  ", ()=>{
    beforeEach(()=>{
        jest.clearAllMocks();
    });

    test("Debe mostrarse correctamente", ()=>{
        expect(wrapper).toMatchSnapshot();
    });
    test("Muestra la tarea en la tabla", ()=>{
        expect(wrapper.find('.p-datatable-tbody').find('tr')).toHaveLength(1);
    });
});
