import {createStore,combineReducers,applyMiddleware,compose} from 'redux';
import thunk from 'redux-thunk';
import { authReducer } from '../reducers/authReducer';
import { tareasReducer } from '../reducers/tareasReducer';
import { uiReducer } from '../reducers/uiReducer';
import { usuariosReducer } from '../reducers/usuariosReducer';
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
const reducers = combineReducers({

   ui:uiReducer,
   usuarios: usuariosReducer,
   auth:authReducer,
   tareas: tareasReducer,
});

export const store = createStore(reducers,composeEnhancers(applyMiddleware(thunk)));