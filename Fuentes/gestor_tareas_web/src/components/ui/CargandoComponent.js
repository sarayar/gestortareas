import React from 'react'

import { ProgressSpinner } from 'primereact/progressspinner';
import {Dialog} from 'primereact/dialog';
import {useSelector} from 'react-redux';
export const CargandoComponent = () => {

  const {loading} = useSelector(state => state.ui);
    return (
        <Dialog showHeader={false}
            visible={loading}
            style={{ width: '20vw' }} closeOnEscape={false}
            dismissableMask={false}
            closable={false}
            onHide={()=>{}}
            modal >
            <div className="row mt-2">
                <div className="col-12 text-center">
                    <h6>Cargando...</h6>
                </div>
            </div>
            <div className="mt-2 d-flex justify-content-center">
                <ProgressSpinner/>
            </div>
            
        </Dialog>
    )
}
