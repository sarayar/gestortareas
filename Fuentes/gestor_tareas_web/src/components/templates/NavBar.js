import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link} from 'react-router-dom'
import { cerrarSesion } from '../../actions/authActions';
export const NavBar = () => {
    const dispatch = useDispatch();
    const handleLogout = (e) => {
        dispatch(cerrarSesion());
    };
    const { usuario } = useSelector(state => state.auth);

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="./tareas">
                        <div className="d-inline-block ms-5">Gestor de Tareas</div>
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                      
                            <li className="nav-item dropdown">
                                <button className="nav-link dropdown-toggle btn btn-link"  id="navbarDropdown"  data-bs-toggle="dropdown" aria-expanded="false">
                                    Tareas
                                </button>
                                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                    
                                    <li>
                                        <Link className="dropdown-item" to="/tareas" >Ver</Link>
                                    </li>
                                    
                                </ul>
                            </li>
                        </ul>
                        <div className="d-flex">
                            <div className="row">
                                <div className="text-white text-center  col-12 d-flex flex-column justify-content-center" >
                                    <h6 >{`Bienvenido ${usuario?.nombre}`}</h6>
                                    <button type="button" onClick={handleLogout} className="btn btn-link link-light" >Cerrar Sesión</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </>
    )
}
