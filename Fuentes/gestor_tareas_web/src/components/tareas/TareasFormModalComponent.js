import React, { useEffect, useState } from 'react'

import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputText} from 'primereact/inputtext';
import { InputSwitch } from 'primereact/inputswitch';
import { Inplace ,InplaceDisplay,InplaceContent} from 'primereact/inplace';
import './TareasModal.scss';
import { useDispatch,useSelector } from 'react-redux';
import {  ocultarModalTarea, startActualizarTarea, startAgregarTarea } from '../../actions/tareasActions';

export const TareasFormModalComponent = ({ tareaActual = null, mostrar}) => {

    const tituloVentana = !tareaActual? 'Registrar Tarea':`Editar Tarea ${tareaActual.identificador}`;
    const textoBoton = !tareaActual?'Registrar':'Actualizar';
    const claseBoton = !tareaActual?'primary':'warning';
    const [tarea, setTarea] = useState(!tareaActual?{id:'',descripcion:''}:tareaActual);
    const [erroresForm, setErroresForm] = useState({
        descripcion:''
    });
    const  dispatch = useDispatch();
    const {mostrarModal} = useSelector(state => state.tareas);
    useEffect(() => {
        if(!mostrarModal){
            setTarea(!tareaActual?{descripcion:'',vigente:false}:tareaActual);
        }
        setErroresForm({
            descripcion:''
        });
    }, [tareaActual,mostrarModal,setTarea])
    useEffect(() => {
        setTarea(!tareaActual?{id:'',descripcion:'', vigente:false}:tareaActual);
    }, [tareaActual,setTarea]);
    const esFormValido=()=>{
        
        if(tarea.descripcion.trim() === ''){
            setErroresForm({...erroresForm,descripcion:'Debe Ingresar Descripción'});
            return false;
        }else{
            setErroresForm({...erroresForm,descripcion:''});
            return true;
        }
    };
    const onButtonClick = () => {

        if(esFormValido()){
            
            if(tareaActual == null){
                dispatch(startAgregarTarea(tarea));
            }else {
                dispatch(startActualizarTarea(tarea));
            }
        }
        
    };
    const onCancelarUpdate = () => {
        dispatch(ocultarModalTarea())
    }


    const footer = (
        <div>
            <Button id="boton-accion" label={textoBoton} className={`p-button-${claseBoton}`} icon="pi pi-check" onClick={onButtonClick} />
            <Button label="Cancelar" icon="pi pi-times" className="p-button-secondary" onClick={onCancelarUpdate} />
        </div>
    );



    return (
        <Dialog header={tituloVentana} footer={footer} visible={mostrar}
            style={{ width: '50vw' }} closeOnEscape={false} onHide={() => dispatch(ocultarModalTarea())}
            dismissableMask={false}
            modal >
            <div className="row tareas-modal">
                <div className="col-12">
                    <div className="form-group mb-3" >
                        <h5>Descripción</h5>
                        {(tareaActual)?
                        <div>
                        <Inplace closable  >
                            <InplaceDisplay>
                                {tarea.descripcion}
                            </InplaceDisplay>
                            <InplaceContent>
                                <InputText value={tarea.descripcion} id="descripcion" className={erroresForm.descripcion!==''? 'p-invalid': ''} onChange={(e)=>setTarea({...tarea,descripcion:e.target.value})} />
                                
                            </InplaceContent>
                        </Inplace>
                        {erroresForm.descripcion !=='' && <small id="descripcion-help" className="p-error ">{erroresForm.descripcion}</small>}
                        </div>: 
                        <div>
                            <input type="text" name="descripcion" className={`form-control ${erroresForm.descripcion !=='' && 'is-invalid'}`} value={tarea.descripcion}
                                onChange={(e)=>setTarea({...tarea,descripcion:e.target.value})}></input>
                            {erroresForm.descripcion !==''
                                && <div className="invalid-feedback">
                                {erroresForm.descripcion}
                            </div>}
                        </div>
                        }
                     </div>
                     <div className="mb-3">
                        <h5>¿Está Vigente?</h5>
                        <InputSwitch checked={tarea.vigente} onChange={(e)=>setTarea({...tarea,vigente:e.value})} ></InputSwitch>
                     </div>
                </div>
            </div>
        </Dialog>
    )
}
