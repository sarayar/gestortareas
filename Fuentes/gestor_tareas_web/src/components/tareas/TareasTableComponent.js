import React from 'react'
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { startEliminarTarea} from '../../actions/tareasActions';
import { useDispatch } from 'react-redux';
import Swal from 'sweetalert2';
import {DateTime} from 'luxon';

export const TareasTableComponent = ({tareas,onEditarTarea= null}) => {

    const dispatch = useDispatch()
    const editarTarea = (tarea)=>{
        if(onEditarTarea != null){
            onEditarTarea(tarea);
        }
    }

    const handleEliminar = async (tarea)=>{
        const respuesta = await Swal.fire({
            title: '¿Esta seguro?',
            text: `¿Desea realmente eliminar la tarea ${tarea.identificador}?`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar',
            cancelButtonText:'Cancelar'
          });
          if (respuesta.isConfirmed) {
            const resultado = await dispatch(startEliminarTarea(tarea));
            if(resultado){
                Swal.fire(
                    'Tarea eliminada',
                    'Tarea eliminada exitosamente',
                    'success'
                  );
            }else {
                Swal.fire('Error', 'Ha ocurrido un error al eliminar', 'error');
            } 
          }
    };

    const actionBodyTemplate = (rowData) => {
        return (
            <React.Fragment>
                <div className="d-flex justify-content-around">
                    <button  className="btn btn-warning" onClick={()=>editarTarea(rowData)}  >Editar</button>
                    <button  onClick={()=>handleEliminar(rowData)} className="btn btn-danger" >Eliminar</button>
                </div>
            </React.Fragment>
        );
    }

    const fechaBodyTemplate = (rowData)=>{
        window.datos = rowData;
        window.luxon = DateTime;
        return (
            <React.Fragment>
                <div >
                    {DateTime.fromISO(rowData.fechaCreacion).toLocaleString(DateTime.DATETIME_FULL)}
                </div>
                
            </React.Fragment>
        );
    }


    const vigenteBodyTemplate = (rowData)=>{
        
        return (
            <React.Fragment>
                <div >
                    {rowData.vigente? 
                        <i className="pi pi-check text-success"></i>
                        : 
                        <i className="pi pi-times text-danger"></i>}
                </div>
                
            </React.Fragment>
        );
    }

    return (
        <div>
            <DataTable value={tareas}>
                <Column field="identificador" header="Id"></Column>
                <Column body={fechaBodyTemplate} header="Fecha de Creación"></Column>
                <Column field="descripcion" header="Descripción"></Column>
                <Column body={vigenteBodyTemplate} header="Está vigente?"></Column>
                <Column body={actionBodyTemplate} header="Acciones"></Column>
            </DataTable>
        </div>
    )
}
