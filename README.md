# Gestor de Tareas

Prueba técnica de fullstack para Coopeuch
## Tecnologías utilizadas

Mongo DB: para capa de persistencia

Spring Boot: api rest

React + Redux: Web

Docker: Despliegue de la aplicación

## Installacion

Se debe construir el compilado de la api rest ingresando al directorio de la misma y construyendo con maven

```bash
cd gestor_tareas_api
./mvnw package
```
Esto permite evaluar el estado de las pruebas unitarias de la aplicación.
Posteriormente se debe construir las imagenes y contenedores docker, desde el directorio raiz

```bash
docker-compose -f docker-compose-prod.yml build
docker-compose -f docker-compose-prod.yml up -d
```
## Uso

Para acceder a la aplicación se hace a travez de 
http://localhost:8082

Las credenciales de acceso del ejemplo son:

usuario: sarayar@skunux.cl
pass: 123456

## Requisitos
Para desplegar la aplicación se requiere tener instalado docker y docker-compose

## Autor
Sebastián Araya R. sarayar@skynux.cl